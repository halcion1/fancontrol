# Reverse Engineering an RF Remote - Replace with cheap IoT computer #

I purchased [this Harbor Breeze Kingsbury fan](http://www.amazon.com/Harbor-Breeze-Platinum-Kingsbury-Technology/dp/B00UKHMWEA/ref=as_li_ss_il?ie=UTF8&linkCode=li2&tag=dataworlds-20&linkId=7b79cbc3bafd3bb4c36a30941904bdf2) a while back.  While it’s a nice looking fan, and quite large, I was disappointed to find that it is controlled with a remote.  Main reason being, it is not possible to control the fan without a remote.  And the problem with a remote is that it inevitably gets lost and/or broken.  So, I decided to undertake a project to replace the remote.

I break down this project into a couple of challenges:

1. Capture what the remote control is sending to the fan

2. Analyze the signal and determine pattern

3. Figure out how to recreate the pattern

4. Transmit corresponding signal with Radio Frequency (RF) Transmitter (TX)

5. Make it easy to have this control available on my WiFi network

6. Put a nice UI on top of it so it’s very user friendly.

## Hardware:

* [NodeMCU Microcontroller](http://amzn.to/1RCbnVV).  It costs a bit more than a simple [ESP8266](http://amzn.to/22sRnNw) but it is very easy to upload code and it also converts a normal USB voltage to 3.3v for the ESP.  Alternatively you can get these direct from chinese suppliers for very cheap, if you don’t mind waiting a couple of weeks.

* [315 Mhz transmitter](http://amzn.to/1sVFbcb)

* [RTL-SDR](http://amzn.to/22sRd8Q) Software defined radio dongle

* [Harbor Breeze Kingsbury Fan](http://amzn.to/22sRqsr)

* Some [Breadboard Wire](http://amzn.to/1VocwaC) (one of these will serve as the antenna as well)

## Software:

#### I am on OSX but most of this software (or functional equivalent) is available cross platform:

* [Gqrx](http://gqrx.dk) Software Defined Radio (SDR)

* rtl_fm - was installed with Gqrx, but needed to create a symbolic link to use it easily from command line:  

**sudo ln -s /Applications/Gqrx.app/Contents/MacOS/rtl_fm /usr/local/bin/rtl_fm **

* [Sox Sound eXchange](http://sox.sourceforge.net) - swiss army knife utility for sound processing

* [Audacity](http://www.audacityteam.org) audio editor 

* [Platformio](http://platformio.org) for editing, building, and uploading code



## Conclusion

Using these tools and techniques, it should be possible to reverse engineer many 315 Mhz and 433 Mhz remote controls.  

Controlling these with cheap computers with REST and Web interfaces allows you to bring these things into Internet of Things where they can be remotely controlled, monitored, and potentially exploited -- well, that’s another topic.

Thanks to [Samy Kamkar](http://samy.pl/) for some ideas on the approach.